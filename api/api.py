from django.http import request
from rest_framework.response import Response
from .serializers import EmailRequestSerializer
from rest_framework.views import APIView
from rest_framework import status
from rest_framework import permissions
from rest_framework.decorators import permission_classes

permission_classes((permissions.AllowAny,))


class EmailService(APIView):
    def post(self, request):
        serializer = EmailRequestSerializer(data=request.data)

        if serializer.is_valid():
            email = serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
