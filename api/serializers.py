from django.http import request
from rest_framework import serializers
from api.model import *
from django.views.decorators.csrf import csrf_exempt

# Serializers define the API representation.
class EmailRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailRequest
        fields = "__all__"

    # def save(self, validate_data):
    #     instance = EmailRequest()
    #     instance.full_name = validate_data.get("full_name")
    #     instance.title = validate_data.get("title")
    #     instance.subject = validate_data.get("subject")
    #     instance.phone = validate_data.get("phone")
    #     instance.save()
    #     return instance
