from django.http.response import JsonResponse
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from api.serializers import *

from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from config import settings

# # ViewSets define the view behavior.


class EmailRequestViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = EmailRequest.objects.all()
    serializer_class = EmailRequestSerializer

    @action(detail=False, methods=['post'])
    def sendEmail(self, request):
        try:
            data = request.data
            subject = 'Nuevo mensaje de formulario de contacto'
            message = data.get('message')
            full_name = data.get('full_name')
            phone = data.get('phone')

            template = get_template('mi_template_correo.html')

            content = template.render({
                "message": message, 
                "full_name": full_name,
                "phone": phone})

            message = EmailMultiAlternatives(
                subject=subject,
                body= '',
                from_email = settings.EMAIL_HOST_USER, #Remitente
                to =['jwgarcia003@gmail.com', ] # Destinatarios
            )
            message.attach_alternative(content, 'text/html')
            message.send()

            return JsonResponse({"msg": "Your message has been sent successfully."})
        except Exception as e:
            return JsonResponse({"msg": f"Error: {e.args[0]}"}, status=500)
