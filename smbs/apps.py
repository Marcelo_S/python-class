from django.apps import AppConfig


class SmbsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'smbs'
