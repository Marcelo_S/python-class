from django.db import models
from django.db.models.expressions import F

# Create your models here.


class EmailRequest(models.Model):
    tittle = models.CharField(
        max_length=200, null=False, blank=False, help_text='Email title')
    subject = models.TextField(
        max_length=500, null=True, blank=True, help_text='Your subject')
    full_name = models.CharField(max_length=100, blank=True, null=True)
    phone = models.CharField(max_length=10, blank=True, null=True)
    sent = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    def save(self, *args, **kwargs):
        # logica para enviar el correo
        #if not self.id:
        
        super(EmailRequest, self).save(*args, **kwargs)
