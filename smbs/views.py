from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from smbs.models import EmailRequest

# Create your views here.
# class SendMail(views.DefaultView):


@csrf_exempt
def send_email(request):
    if request.method == 'GET':
        emails = EmailRequest.objects.all().values('id', 'subject', 'tittle')
        return JsonResponse(list(emails), safe=False)
    elif request.method == 'POST':
        import json
        try:
            tittle = request.POST.get('tittle')
            subject = request.POST.get('subject')
            email, created = EmailRequest.objects.get_or_create(
                tittle=tittle, subject=subject)
            if created:
                return JsonResponse({'msg': 'The email already exists'})
            else:
                return JsonResponse({'msg': 'Email created successfully'})
        except Exception as e:
            return JsonResponse({'msg': f'Error: {e.args[0]}'}, status=500)

    else:
        return JsonResponse({'msg': 'Method not allowed'})
